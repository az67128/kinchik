import adapter from '@sveltejs/adapter-auto';
import path from 'path';

const config = {
	kit: {
		adapter: adapter(),
		target: '#svelte',
		vite: {
			resolve: {
				alias: {
					components: path.resolve('./src/components'),
					store: path.resolve('./src/store.js')
				}
			}
		}
	}
};

export default config;
