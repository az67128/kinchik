import parse from 'node-html-parser';

export async function get({ url }) {
	const min_years = url.searchParams.get('min_years') || 1920;
	const max_years = url.searchParams.get('max_years') || new Date().getFullYear();

	const response = await fetch(
		`https://www.kinopoisk.ru/chance/?item=true&not_show_rated=false&count=5&max_years=${max_years}&min_years=${min_years}&rnd=${Math.random()}`
	);

	const json = await response.json();
	const data = json.map((item) => {
		const root = parse(item);
		const id = root.querySelector('.movieBlock').getAttribute('data-id-film');
		const source = root.querySelector('img').getAttribute('src');
		const rating =
			Math.round(Number(root.querySelector('.WidgetStars').getAttribute('value')) * 10) / 10;
		const src = `https://kinopoisk.ru/${source.slice(3)}`;
		const nameRaw = root.querySelector('.filmName a').textContent;
		const name = nameRaw.replace(/&nbsp;/g, ' ');
		const yearRaw = root.querySelector('.filmName span').textContent;
		const year = yearRaw.match(/\(\d{4}\)/)[0].slice(1, -1);
		return { id, src, name, rating, year };
	});

	return {
		body: data
	};
}
